import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// importar componentes

import { HomeComponent } from './components/home.component';
import { ErrorComponent } from './components/error.component';
import { ProductosListComponent } from './components/productos-list.component';
import { ProductosAddComponent } from './components/productos-add.component';
import { ProductosEditComponent } from './components/productos-edit.component';
import { ProductosDetalleComponent } from './components/productos-detalle.component';


const appRoutes:Routes = [
	{path:'',component: HomeComponent},
	{path:'home',component: HomeComponent},
	{path:'productos',component: ProductosListComponent},
	{path:'crear-producto',component: ProductosAddComponent},
	{path:'editar-producto/:id',component: ProductosEditComponent},
	{path:'productos-detalle/:id',component: ProductosDetalleComponent},
	{path:'**',component: ErrorComponent}
];

/*esto lo necesita angular para poder cargar 
los providers de las rutas y que todo funcione*/

export const appRoutingProviders:any[] = [];

/* cargar todas las rutas definidad en appRoutes y las inyecta en la cofiguracion
del framework  */
export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);