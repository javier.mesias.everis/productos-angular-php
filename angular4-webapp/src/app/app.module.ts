import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

//rutas
import { routing , appRoutingProviders } from './app.routing';


//componentes
import { AppComponent } from './app.component';
import { HomeComponent } from './components/home.component';
import { ErrorComponent } from './components/error.component';
import { ProductosListComponent } from './components/productos-list.component';
import { ProductosAddComponent } from './components/productos-add.component';
import { ProductosEditComponent } from './components/productos-edit.component';
import { ProductosDetalleComponent } from './components/productos-detalle.component';

//esto es obligatorio
import { HttpModule } from '@angular/http';
import { HttpClientModule, HttpClient } from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ErrorComponent,
    ProductosListComponent,
    ProductosAddComponent,
    ProductosEditComponent,
    ProductosDetalleComponent

  ],
  imports: [
    BrowserModule,
    FormsModule,
    routing,
    HttpClientModule,
    HttpModule,
  ],
  providers: [appRoutingProviders],
  bootstrap: [AppComponent]
})
export class AppModule { }
