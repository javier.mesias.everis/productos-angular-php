import { Component } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { ProductoService } from '../services/producto.service';
import { Producto } from '../models/producto';
import { GLOBAL } from '../services/global';

@Component({
  selector: 'productos-edit',
  templateUrl:'../views/productos-add.html',
  providers:[ProductoService]
})

export class ProductosEditComponent { 

	public titulo: string;
	public producto: Producto;
	public filesToUpload: Array<any>;
	public resultUpload;
	public is_edit: boolean;
	public url: string;

	constructor(
		private _route:ActivatedRoute,
		private _router: Router,
		private _productoService: ProductoService
		){
		this.titulo = 'Editar un productos';
		this.is_edit = true;
		this.producto = new Producto(0,'','',0,'');
		
		this.url = GLOBAL.url;
	}

	public ngOnInit(){
		this.getProducto();
	}

	public onSubmit(){
		
		if (this.filesToUpload && this.filesToUpload.length >= 1){

			this._productoService.makeFileRequest(GLOBAL.url + 'upload-file' , [] , this.filesToUpload ).then((result) => {
			this.resultUpload = result;
			this.producto.imagen = this.resultUpload.filename;
			this.updateProducto();

			}, (error) => {
					console.log(error);
				});
		}else{
				this.updateProducto();
		}
	}


	updateProducto(){
		
		this._route.params.forEach((params: Params) => {
			let id = params['id'];

			this._productoService.editProducto(id, this.producto).subscribe(
				result => {
						
						if (result.code == 200){
							this._router.navigate(['/productos-detalle',id]);
						}else{
							console.log(result);
						}
				},
				error => {
					var errorMessage = <any>error;
					console.log(errorMessage);
				}
			);


		});

		


	}

	fileChangeEvent(fileInput: any) {
		//agarra todos los ficheros que capturan con el input files y los monta en un array.
		//this.filesToUpload = <Array<Files>>fileInput.target.files;
		this.filesToUpload = fileInput.target.files;
	}


	getProducto()
	{
		this._route.params.forEach((params: Params) => {

			let id = params['id'];

			this._productoService.getProducto(id).subscribe((response) => {
					if(response.code == 200)
					{
						this.producto = response.data;
					}
					else
					{
						this._router.navigate(['/productos']);
					}
			},

			(error) => {
					console.log(error);
			}

			);

		});
	}


}