import { Component } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { ProductoService } from '../services/producto.service';
import { Producto } from '../models/producto';
import { GLOBAL } from '../services/global';

@Component({
  selector: 'productos-list',
  templateUrl:'../views/productos-list.html',
  providers:[ProductoService]
})

export class ProductosListComponent { 

	public titulo: string;
	public productos: Array<any>;
	public url:string =  GLOBAL.url;
	public confirmado= null;

	constructor(
		private _route:ActivatedRoute,
		private _router: Router,
		private _productoService: ProductoService
		){
		this.titulo = 'Listado de productos';
	}

	public ngOnInit(){
		this.getProductos();
	}

	borrarConfirmado(id){
		this.confirmado = id;
	}

	cancelarConfirmado(){
		this.confirmado = null;
	}

	getProductos(){

		this._productoService.getProductos().subscribe(
				result => {
						
						if (result.code != 200){
							console.log("Error en el servidor");
						}else{
							this.productos = result.data;
							
						}
				},
				error => {
					var errorMessage = <any>error;
					console.log(errorMessage);
				}
			);

	}

	onDeleteProducto(id){

			this._productoService.deleteProducto(id).subscribe(
				result => {
						
						if (result.code == 200){
							this.getProductos();
						}else{
							alert("Error");
						}
				},
				error => {
					var errorMessage = <any>error;
					console.log(errorMessage);
				}
			);

	}

}