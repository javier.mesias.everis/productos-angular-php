import { Component } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { ProductoService } from '../services/producto.service';
import { Producto } from '../models/producto';
import { GLOBAL } from '../services/global';

@Component({
  selector: 'productos-detalle',
  templateUrl:'../views/productos-detalle.html',
  providers:[ProductoService]
})

export class ProductosDetalleComponent { 

	public titulo: string;
	public producto: Producto;
	public url:string =  GLOBAL.url;

	constructor(
		private _route:ActivatedRoute,
		private _router: Router,
		private _productoService: ProductoService
		){
		this.titulo = 'Detalle de producto';
	}

	public ngOnInit(){

		this.getProducto();
	}

	getProducto()
	{
		this._route.params.forEach((params: Params) => {

			let id = params['id'];

			this._productoService.getProducto(id).subscribe((response) => {
					if(response.code == 200)
					{
						this.producto = response.data;
					}
					else
					{
						this._router.navigate(['/productos']);
					}
			},

			(error) => {
					console.log(error);
			}

			);

		});
	}

}