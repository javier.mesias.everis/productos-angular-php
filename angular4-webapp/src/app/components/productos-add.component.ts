import { Component } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { ProductoService } from '../services/producto.service';
import { Producto } from '../models/producto';
import { GLOBAL } from '../services/global';

@Component({
  selector: 'productos-add',
  templateUrl:'../views/productos-add.html',
  providers:[ProductoService]
})

export class ProductosAddComponent { 

	public titulo: string;
	public productos: Array<any>;
	public producto: Producto;
	public filesToUpload: Array<any>;
	public resultUpload;

	constructor(
		private _route:ActivatedRoute,
		private _router: Router,
		private _productoService: ProductoService
		){
		this.titulo = 'Crear un nuevo productos';
		this.producto = new Producto(0,'','',0,'');
	}

	public ngOnInit(){
		console.log(this.titulo);
	}

	public onSubmit(){
		//console.log(this.filesToUpload);
		if (this.filesToUpload && this.filesToUpload.length >= 1){

			this._productoService.makeFileRequest(GLOBAL.url + 'upload-file' , [] , this.filesToUpload ).then((result) => {
			this.resultUpload = result;
			this.producto.imagen = this.resultUpload.filename;
			this.saveProducto();

			}, (error) => {
					console.log(error);
				});
		}else{
				this.saveProducto();
		}
	}


	saveProducto(){

		this._productoService.addProducto(this.producto).subscribe(
				result => {
						
						if (result.code == 200){
							this._router.navigate(['/productos']);
						}else{
							console.log(result);
						}
				},
				error => {
					var errorMessage = <any>error;
					console.log(errorMessage);
				}
			);


	}

	fileChangeEvent(fileInput: any) {
		//agarra todos los ficheros que capturan con el input files y los monta en un array.
		//this.filesToUpload = <Array<Files>>fileInput.target.files;
		this.filesToUpload = fileInput.target.files;
	}


}