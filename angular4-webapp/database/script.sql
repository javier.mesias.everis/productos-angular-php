/*
SQLyog Community v12.5.1 (64 bit)
MySQL - 10.1.30-MariaDB : Database - curso_angular4
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`curso_angular4` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `curso_angular4`;

/*Table structure for table `productos` */

DROP TABLE IF EXISTS `productos`;

CREATE TABLE `productos` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) DEFAULT NULL,
  `descripcion` text,
  `precio` varchar(255) DEFAULT NULL,
  `imagen` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

/*Data for the table `productos` */

insert  into `productos`(`id`,`nombre`,`descripcion`,`precio`,`imagen`) values 
(17,'Celular 4g Cuad Core 1 Gb Ram 8 Gb Interna','Un celular potente y con gran diseÃ±o que te permite lograr mÃ¡xima productividad y diversiÃ³n!\n\nFicha TÃ©cnica\nORIGEN DEL PRODUCTO ARGENTINA\nALTO EN CM 14.00\nANCHO EN CM 7.00\nPROFUNDIDAD EN CM 0.70\nPESO EN KG 0.17\nMESES DE GARANTÃA 12\nTECNOLOGÃA 4G\nTIPO CPU QUAD CORE\nTAMAÃ‘O DE PANTALLA 5\"','1899','image-1518320876-te2356.jpg'),
(18,'Celular Libre Samsung Galaxy J2 Prime Black','CELULAR LIBRE SAMSUNG GALAXY J2 PRIME BLACK\n\nModelo: GALAXY J2 PRIME NEGRO\n____________________________\n\nExperimentÃ¡ su diseÃ±o\nEl Samsung Galaxy J2 Prime cuenta con un cÃ³modo y ergonÃ³mico diseÃ±o de marco suave y redondeado. Gracias a su prÃ¡ctico diseÃ±o y su pantalla touch de 5 pulgadas HD (resoluciÃ³n 540 x 960), ofrece un agarre confortable.','4299','image-1518321330-celular-libre-samsung-galaxy-j2-prime-black-D_NQ_NP_663825-MLA25503914233_042017-F.jpg'),
(19,'Celular Samsung Galaxy ACE 4 NEO 3G Blanco','El Samsung Galaxy Ace 4 3G es un sencillo smartphone Android 4.4 KitKat con una pantalla WVGA de 4 pulgadas, procesador dual-core a 1,2GHz, cÃ¡mara de 5 megapixels, cÃ¡mara frontal VGA, 512MB de RAM, 4GB de almacenamiento interno y conectividad 3G.','8500','image-1518321474-celular-samsung-galaxy-ace-4-neo-3g-blanco.jpg'),
(20,'CELULAR LIBRE SAMSUNG A9 smartphone ','El Samsung Galaxy A9 es el smartphone mÃ¡s poderoso en cuanto a caracterÃ­sticas y mÃ¡s grande de la serie Galaxy A, con una pantalla Full HD de 6 pulgadas, procesador octa-core Snapdragon 652, 3GB de RAM, 32GB de almacenamiento interno expandible, cÃ¡mara principal de 13 megapixels, cÃ¡mara frontal de 8 megapixels, lector de huellas dactilares y baterÃ­a de 4000 mAh, corriendo Android 5.1.1 Lollipop.','18500','image-1518321786-te2349.png');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
