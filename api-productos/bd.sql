create database if not EXISTS curso_angular4;
use curso_angular4;

create table productos(
	id int(255) auto_increment not null,
	name varchar(255),
	description text,
	precio varchar(255),
	imagen varchar(255),
	constraint pk_productos primary key(id)
)engine=InnoDb;