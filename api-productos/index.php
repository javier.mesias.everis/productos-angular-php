<?php
require_once 'vendor/autoload.php';

$app = new \Slim\Slim();

$db = new mysqli('localhost','root','','curso_angular4');

header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
header("Allow: GET, POST, OPTIONS, PUT, DELETE");
$method = $_SERVER['REQUEST_METHOD'];
if($method == "OPTIONS") {
    die();
}

$app->get("/pruebas", function() use($app , $db){

	echo "esto es un prueba : Hola Mundo.";


});


$app->get("/productos", function() use($app , $db){

	$result = array();
	$sql = "SELECT * FROM productos ORDER BY id DESC";
	$query = $db->query($sql);
	$productos = array();
	while ($producto = $query->fetch_assoc()) {
		$productos[] = $producto;
	}

	$result = array(
				'status'=>'success',	
				'code'=>'200',
				'data'=>$productos,
			);

	echo json_encode($result);

});

$app->get("/producto/:id", function($id) use($app , $db){

	
	$sql = "SELECT id, name as nombre, description as descripcion, precio, imagen  FROM productos WHERE id = '$id' ";
	$query = $db->query($sql);
	
	$result = array(
				'status'=>'error',	
				'code'=>'404',
				'message'=>'producto no disponible'
			);


	if ($query->num_rows == 1) {
		$productos = $query->fetch_assoc();
		$result = array(
				'status'=>'success',	
				'code'=>'200',
				'data'=>$productos
			);
	}

	echo json_encode($result);

});


$app->get("/delete-producto/:id", function($id) use($app , $db){

	
	$sql = "DELETE  FROM productos WHERE id = '$id' ";
	$query = $db->query($sql);
	
	
	if ($query) {
		$result = array(
				'status'=>'success',	
				'code'=>'200',
				'message'=>'El producto se ha eliminado.'
			);
	}else{

		$result = array(
				'status'=>'error',	
				'code'=>'404',
				'message'=>'El producto no se ha eliminado.'
			);

	}

	echo json_encode($result);

});


//{ "name":"Teclado","description":"Teclado Inalambrico","precio":"1500"  }

$app->post("/productos", function() use($app , $db){


	$json = $app->request->post('json');
	$data = json_decode($json);


	if (!isset($data->nombre))
	{
		$data->nombre = null;
	}

	if (!isset($data->descripcion))
	{
		$data->descripcion = null;
	}

	if (!isset($data->precio))
	{
		$data->precio = null;
	}

	if (!isset($data->imagen))
	{
		$data->imagen = null;
	}

	$query = "INSERT INTO productos (name,description,precio,imagen)";
	$query .= " VALUES ('".$data->nombre."','".$data->descripcion."','".$data->precio."','".$data->imagen."')  ";
	
	$insert = $db->query($query);

	$result = array(
				'status'=>'error',	
				'code'=>'404',
				'message'=>'producto no se ha creado',
			);

	if ($insert){
		$result = array(
				'status'=>'success',	
				'code'=>'200',
				'message'=>'producto creado correctamente',
			);
	}

	echo json_encode($result);

});


$app->post("/update-producto/:id", function($id) use($app , $db){


	$json = $app->request->post('json');
	$data = json_decode($json);

	$query = "UPDATE  productos SET ";
	$query .= "name = '".$data->nombre."',";
	if (isset($data->imagen)){
		$query .= "imagen = '".$data->imagen."',";
	}
	$query .= "description = '".$data->descripcion."',";
	$query .= "precio = '".$data->precio."'";
	$query .= " WHERE id = '".$id."' "; 

	$update = $db->query($query);

	if ($update){
		$result = array(
				'status'=>'success',	
				'code'=>'200',
				'message'=>'producto actualizado correctamente',
			);
	}else{
		$result = array(
				'status'=>'error',	
				'code'=>'404',
				'message'=>'producto no se ha actualizado',
			);
	}

	echo json_encode($result);

});


$app->post("/upload-file", function() use($app , $db){

	if (isset($_FILES['uploads'])){
		$piramedeUploader = new PiramideUploader();
		$upload = $piramedeUploader->upload('image','uploads','uploads', array('image/jpeg','image/png'));
		$file = $piramedeUploader->getInfoFile();
		$file_name = $file['complete_name'];
		
		if (isset($upload) && $upload['uploaded'] == false )
		{
			$result = array(
				'status'=>'error',	
				'code'=>'404',
				'message'=>'La imagen NO se ha subido',
			);

		}else{

			$result = array(
				'status'=>'success',	
				'code'=>'200',
				'message'=>'La imagen se ha subido',
				'filename' => $file_name
			);

		}
	}
	
	echo json_encode($result);

});



$app->run();


?>