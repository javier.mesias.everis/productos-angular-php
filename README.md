### Manual de instalacion

# Aplicacion de gestión Productos usando en el front Angular4 y en el back una Api Rest con PHP

# Prerequisitos
> Tener instalado:  php , nodejs 


# Pasos para instalar en un ambiente local

- Clonar el repositorio

```
git clone https://gitlab.com/javier.mesias.everis/productos-angular-php.git
```

## Instrucciones para levantar la api programada con PHP

- configuracion del  virtual host para levantar el api de PHP:

```
	<VirtualHost *:80>    
	    DocumentRoot "c:/xampp_56/htdocs/productos-angular-php/api-productos"
	    ServerName api-productos.local
	    ServerAlias api-productos.local
	    <Directory "c:/xampp_56/htdocs/productos-angular-php/api-productos">
	        Options Indexes FollowSymLinks      
	        AllowOverride All
	        Order Deny,Allow
	        Allow from all      
	    </Directory>  
	</VirtualHost>
```

- Ingresar a /api-productos y ejecutar: 

```
composer install
```

- Ejecutar el script bd.sql para instalar la base de datos.

- Reinciar servidor de apache.


## Instrucciones para levantar la APP Angular4


- Ingresar a angular4-webapp y ejecutar

```
npm install
```

- Para levantar la app local en modo desarrollo ejecutamos :

```
npm run ng serve
```
 

Esto levantara el la aplicacion cliente react en el puerto 4200 [http://localhost:4200/](http://localhost:4200/). 


## Fin instalación local
